## Antes de Comenzar!

Es importante tener instalado git.

[Dejo link para instalar git en tu Mac](https://git-scm.com/download/mac)

## ¿Cómo instalar Zsh?

Para instalar Zsh debemos ejecutar estos comandos (dependiendo de la plataforma)

**macOS**: En las últimas versiones ya viene instalado por defecto **Ubuntu**: `sudo apt-get install zsh` **Windows**: Aquí entendemos que estamos usando la Windows Terminal y abriendo una pestaña en modo “Ubuntu”. Ejecutaremos `sudo apt-get install zsh` también para instalarlo.

## ¿Cómo hacer que Zsh sea mi shell por defecto?

Para ello, sin importar la plataforma, ejecutaremos: `chsh -s $(which zsh)`.

## ¿Cómo nos aseguramos de estar usando Zsh?

Al ejecutar el comando `echo $0` hemos de ver algo similar a `/bin/zsh`. Si todo es así significa que estamos utilizando Zsh correctamente.

##### Ficheros de configuración

.zshrc => Donde se pone todo, como funciones y de mas

.zlogin => Configuraciones de entorno gráfico, no se usa.

 .zshenv=> Todos los exporta, todas las variables globales que necesitemos exportar. 

##### **Quitar el mensaje en la terminal por defecto de ultimo login**

`touch .hushlogin`

#### Alias

##### Nuestros Alias

1. ```bash
   alias sudo="sudo "
   alias ll="ls -l"
   alias la="ls -la"     (Ver archivos oculto)
   alias tky="cd $HOME/Documents/trossky.dev"
   alias cdt="tky;cd training"
   alias cdw="tky;cd tky"
   alias gs="git status"
   alias gd="git diff"
   alias gc="git commit -m "
   alias gp="git push -u origin"
   ```

##### Alias !!

El Alias !!, lo que permite poner en determinado momento el anterior comando.

Ejemplo

```bash
ls
sudo !! ---Equivalente--> sudo ls
```



##### Como usar SUDO con Alias

Un truco es crear nuestro [alias sudo="sudo"]() y sale.

#### Funciones

#####  Function docker_list

```bash
function docker_list{
  containers=$(docker ps | awk '{if (NR!=1) print $1 ": " $(NF)}')

  echo "👇 Containers 👇"
  echo $containers
}
```

##### Function gc

```bash
function gc {
  git add -A

  if [ -z "$1" ]; then
    git commit -S
  else
    git commit -S -m"$1"
  fi
}
```

##### Function mysql_general_log

Para activar el General Log y poder ver todos los Queries que se están ejecutando en tiempo real.

```bash
function mysql_general_log{
  MYSQL_LOG_PATH="var/log/mysql_general.log"
  
  sudo rm $MYSQL_LOG_PATH
  sudo touch $MYSQL_LOG_PATH
  sudo chmod 777 $MYSQL_LOG_PATH
  
  mkdir -p /usr/local/etc/my.cnf.d
  mysql -uroot -e "SET GLOBAL general_log = 'ON';"
  mysql -uroot -e "SET GLOBAL general_log_file= '$MYSQL_LOG_PATH'"
  
  tail -f $MYSQL_LOG_PATH
}
```



#### Reverse Search

```bash
bindkey -l
bindkey -m main
control + R
du -h directory (Ver tamaño de la carpeta)
command+shif+.  (Ver archivos ocultos en carpeta)
```

##### Crear widget o Atajos de teclado para comandos

* Creamos una función con un comando cualquiera

  ```bash
  _display_message(){
    echo -n "ls -la "
  }
  ```

* Ahora creamos el widget

  ```bash
  zle -N _display_message
  ```

* Por ultimo creamos un bindkey para la combinación de teclado que consideremos, para este ejemplo es Control+h.

  ```bash
  bindkey "^h" _display_message
  ```

#### FZF

Es una herramienta que pone un poco de interactividad en la terminal.

```bash
fzf
```



#### Reverse Search + FZF = Productividad

El commando 

```bash
history es igual a fc -rl
 fc -rl | fzf
```

***Quitar una columna al resultado de un comando en unix*** usando AWKA

```bash
fc -rl 1 | awk '{$1="";print substr($0,2)}'
```

###### Resultado Final

```bash
_reverse_search(){
  local selected_command=$(fc -rl 1 | awk '{$1="";print substr($0,2)}' | sort --unique | fzf)
  LBUFFER=$selected_command
}

zle -N _reverse_search
bindkey "^r" _reverse_search
```

###### Reto

Crear un atajo donde se cree una carpeta, acceda a ella y ejecute un comando ls -la



#### Crear un Tema

Para probar primero la prompt que estamos construyendo la podemos imprimir para ver que tal luce.

```bash
print -P '%n @ %d' Probar el commando
PROMPT='%n @ %d👇 ' Asignar el valor a PROMPT, y para que el cambio sea permanente lo dejamos en el archivo .zshrc.sh.
```

###### Git info versiones superiores a 20

```bash
function git_prompt_info {
  inside_git_repo="$(git rev-parse --is-inside-work-tree 2>/dev/null)"

  if [ "$inside_git_repo" ]; then
    current_branch=$(git branch --show-current)
    print -P " on %{%F{yellow}%}$current_branch%{%f%}"
  else
    echo ""
  fi
}
```

###### Git info versiones inferiores a 20

```bash

function parse_git_branch() {
  git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* .∗/ (\1)/'
}


function git_prompt_info {
  inside_git_repo="$(git rev-parse --is-inside-work-tree 2>/dev/null)"
  if [ "$inside_git_repo" ]; then
    current_branch=$(parse_git_branch)
    print -P " on %%}$current_branch%"
  else
    echo ""
  fi
}
```

###### Comando del lado derecho con RPROMPT

```bash
RPROMPT="%T"
```

#### Instalar Oh My Zsh

Framework para Zsh

#### TIME Saber el tiempo de carga de zsh

```bash
time zsh -i -c exit
```

##### Creamos un test de tiempo 

```bash
seq 1 10
for i in $(seq 1 10); do time zsh -i -c exit; done
```



##### Themas Oh My Zsh

```bash
# See https://github.com/ohmyzsh/ohmyzsh/wiki/Themes
ZSH_THEME="amuse"
```

##### Plugins   Oh My Zsh

1. https://github.com/zsh-users/zsh-autosuggestions/blob/master/INSTALL.md
2. https://github.com/zsh-users/zsh-syntax-highlighting/blob/master/INSTALL.md

####  Optimizando  Oh My Zsh 

Mejorar los tiempos de apertura de tu Iterm

* No hacer login, simplemente pasar el comando de zsh como */bin/zsh*
*  Non-Interactive
* Lograr el tiempo de respuesta menos a 50 ms

#### Problemas con Zsh 

Algo similar a esto **Insecure completion*-*dependent directories detected**

Debes agregar en la linea 72 de ser posible la siguiente variable en true

```bash
ZSH_DISABLE_COMPFIX="true"
```

### ZIM

#### Migrar a ZIM desde Oh my Zsh

Un framework igual al anterior de OhMyZsh

* Desinstalar OhMy Zsh

  ```bash
  uninstall_oh_my_zsh
  ```

* Reiniciar la terminal

* Ahora instalar ZIM, [Ver Documentación](https://github.com/zimfw/zimfw)

* Luego ejecutar:

  ```bash
  zimfw
  ```

* Para instalar plugins

  ```bash
  zimfw uninstall
  zimfw install
  ```

* Reto, instalar un tema de ZIM basados en la documentación oficial.

### Thema para  ZIM
* Ahora instalar Thema para ZIM, [Leer tuto](https://dev.to/christopherjael/como-personalizar-tu-terminal-utilizando-oh-my-zsh-con-powerlevel10k-4bdi)

### Ultima milla

#### Crear funciones Lazy

Puede existir funciones que tardan mucho tiempo y afecta el tiempo de la terminal, pero lo que se puede hacer es dejar estas funciones un poco Lazy, de esta manera podemos arrancar a usar nuestra terminal mientras estas funciones se vayan cargando luego.

Ejemplo:

1. Tenemos este código que tarda 2 segundos

   ```bash
   echo "Begin!"
   sleep 2
   echo "End!"
   ```

2. Ponerlo en una función

   ```bash
   function algo_que_tarda(){
     echo "Begin!"
     sleep 2
     echo "End!"
   }
   ```

3. Extraer la función a un nuevo fichero, pero importarlo desde .zshrc con ***source***. Esto en caso que sea más de una linea.

   ```bash
   source algo_que_tarda.sh
   ```

   Pero aún no se carga de modo asíncrono, ósea en modo **Lazy**.

4. Ahora para que sea Lazy.

   1. Para el caso de funciones o de mas de una linea se realiza el  *source "$HOME/algo_que_tarda.sh"*

   ```bash
   function aqt{
     fname=$(declare -f -F algo_que_tarda)
   
     [ -n "$frame"  ] || source "$HOME/algo_que_tarda.sh"
   
     algo_que_tarda
   }
   ```

   2. Para cuando es una sola linea, o cuando sea un simple *eval*

      ```bash
      function aqt{
        fname=$(declare -f -F algo_que_tarda)
      
        [ -n "$frame"  ] || eval $()
      
        algo_que_tarda 
      }
      ```

   De esta forma, hasta que ejecutemos **aqt**, carga por primera vez la función algo_que_tarda y no la vuelve a cargar en nuestra terminal por lo menos por esa sessión. Y de esta forma cargaremos algo_que_tarda en modo **lazy**

   1. Ejecuto ***algo_que_tarda***
   2. Ejecuto ***aqt***
   3. Nuevamente ***algo_que_tarda***

   

#### Crear barra de carga

Esto sirve para las funciones que tardan mucho tiempo.

[Links de interes](https://stackoverflow.com/a/39898465) 

[Video de barra de carga](https://stackoverflow.com/questions/238073/how-to-add-a-progress-bar-to-a-shell-script/39898465#39898465)



#### Programas extras

* Instalar **[Exa](https://the.exa.website/install/macos)**

* Luego creas un alias como este. 

  ```bash
  alias ls="exa"
  ```

#### Como mantener actualizado los framework

* Oh my Zsh:

  ```bash
  upgrade_oh_my_zsh
  ```

* ZIM

  ```
  zimfw update && zimfw upgrade
  ```

  

* Zsh

  ```bash
  apt-get update, brew update 
  ```

#### Dotfiles

https://github.com/CodelyTV/dotfiles

Añadirle mas poder a tu terminal haciendo uso de dotfiles.

#### Ver información del comando con WICH

`which ll`

Tambien con **Command**

```bash
command -v ll
```



## Coding Final

Este código esta en el archivo ***.zshrc***

```bash
setopt PROMPT_SUBST

alias sudo="sudo "
alias ll="ls -l"
alias la="ls -la"
alias tky="cd $HOME/Documents/trossky.dev"
alias cdt="tky;cd training"
alias cdw="tky;cd tky"
alias gs="git status"
alias gd="git diff"
alias gp="git push -u origin"

export DOCKER_FORMAT="ID\t{{.ID}}\nNAME\t{{.Names}}\nPORT\t{{.Ports}}\nStatus\t{{.Status}}\nCOMMAND\t{{.Command}}\nCREATED\t{{.CreatedAt}}\nSize\t{{.Size}}\n"

function docker_list {
  containers=$(docker ps | awk '{if (NR!=1) print $1 ": " $(NF)}')

  echo "👇 Containers 👇 "
  echo $containers
}

function docker_ps {
  docker ps --format=$DOCKER_FORMAT
}

function gc {
  git add -A
  
  if [ -z "$1" ]; then 
    git commit -S
  else
    git commit -S -m"$1"
  fi
}	
_display_message(){
  echo -n "ls -la "
}

zle -N _display_message
bindkey "^h" _display_message

_get_dir(){
  dirtomove=$(ls | fzf)
  cd "$dirtomove"
}

zle -N _get_dir
bindkey "^b" _get_dir


_reverse_search(){
  local selected_command=$(fc -rl 1 | awk '{$1="";print substr($0,2)}' | sort --unique | fzf)
  LBUFFER=$selected_command
}

zle -N _reverse_search
bindkey "^r" _reverse_search


function prompt_exit_code(){
  local EXIT="$?"

  if [ $EXIT -eq 0 ]; then 
    echo -n green
  else
    echo -n red
  fi
}   

PROMPT='%{%F{$(prompt_exit_code)}%}%n%{%f%} @ %d$ '

function aqt {
  fname=$(declare -f -F algo_que_tarda)

  [ -n "$frame"  ] || source "$HOME/algo_que_tarda.sh"

  algo_que_tarda 
}

source "$HOME/progress-bar.sh"
```



 
