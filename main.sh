#!/bin/zsh

# Generate key ssh to System Control Version(SCV)
ssh-keygen -t rsa -b 2048


# install ZIM
curl -fsSL https://raw.githubusercontent.com/zimfw/install/master/install.zsh | zsh

# install SDK Man
curl -s "https://get.sdkman.io" | bash

cd src
# Configure alias
chmod +x terminals/terminal.sh
cd terminals/
echo "1 Copy alias by user to ~/.terminals directory"
./terminal.sh
cd ..

echo "2 Copy github configurations by user."
cd github &&  ls -a && cp -r .gitconfig  .gitignore_global  ~/ && cd ..

echo "3 Config .zshenv file from zsh package"
chmod +x zsh/zshenv.sh
./zsh/zshenv.sh
source ~/.zshenv

echo "4 Config .zshrc file from zsh package"
chmod +x zsh/zshrc.sh
./zsh/zshrc.sh
source ~/.zshrc

ls -a
cd ..
