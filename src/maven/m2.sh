#!/bin/sh

# shellcheck disable=SC2028
if [ -d ~/.m2 ]; then
  cp settings.xml ~/.m2
else
  print "404: ~/.terminals/alias/personal not found."
fi