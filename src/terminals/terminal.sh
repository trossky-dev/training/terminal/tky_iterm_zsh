#!/bin/sh

# shellcheck disable=SC2028
if [ -d ~/.terminals ]; then
  rm -rf ~/.terminals
else
  print "404: ~/.terminals not found directory."
fi
cp -rf .terminals ~/.terminals