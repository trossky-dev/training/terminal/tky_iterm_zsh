#!/bin/sh

# shellcheck disable=SC2028
echo """
#JFROG credentials
export ARTIFACTORY_READER_USER=\"none\"
export ARTIFACTORY_READER_API_KEY=\"None\"
""" >> ~/.zshenv