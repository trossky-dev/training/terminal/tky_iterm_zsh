#!/bin/sh

# shellcheck disable=SC2028
echo """
#Load alias from personal use
if [ -f ~/.terminals/alias/personal ]; then
  source ~/.terminals/alias/personal
else
  print \"404: ~/.terminals/alias/personal not found.\"
fi

# Load alias from git
if [ -f ~/.terminals/alias/git ]; then
  source ~/.terminals/alias/git
else
  print \"404: ~/.terminals/alias/git not found.\"
fi

# Load alias from AWS
if [ -f ~/.terminals/alias/aws ]; then
  source ~/.terminals/alias/aws
else
  print \"404: ~/.terminals/alias/aws not found.\"
fi

""" >> ~/.zshrc